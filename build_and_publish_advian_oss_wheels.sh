#!/bin/sh
if [ -z $ADVIAN_OSS_WHEELS ]
then
  export ADVIAN_OSS_WHEELS=$(mktemp -d)
  echo "INFO: ADVIAN_OSS_WHEELS was not set, set to " $ADVIAN_OSS_WHEELS
fi

for PROJECT in $(cat projects.txt)
do
  cd $ADVIAN_OSS_WHEELS
  git clone "https://gitlab.com/${PROJECT}.git" repo
  cd repo
  poetry build
  twine upload dist/* || true
  cd ..
  rm -rf repo
done
